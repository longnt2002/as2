﻿    using Microsoft.EntityFrameworkCore;

namespace SaleManagement.Models
{
    public class SaleManagementContext : DbContext
    {
        public SaleManagementContext(DbContextOptions<SaleManagementContext> options) : base(options) { }

        #region DBSet

        public DbSet<Branch> Branches { get; set; }

        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(GetConnectionString());
            }
        }
        private string GetConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", true, true)
            .Build();
            var strConn = config["ConnectionStrings:SaleManagementDB"];

            return strConn;
        }
    }
}
