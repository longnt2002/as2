﻿using Microsoft.AspNetCore.Mvc;
using SaleManagement.DTO;
using SaleManagement.Models;
using SaleManagement.Repositories;

namespace SaleManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController
    {
        private readonly IBranchRepository _repo;
        public BranchesController(IBranchRepository repository)
        {
            _repo = repository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Branch>>> GetAllBranches() { 
            return await _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<List<Branch>>> GetBranchById(int id)
        {
            var branch = await _repo.GetById(id);
            if (branch != null)
            {
                return branch;
            }
            return null;
        }

        [HttpPost] 
        public async Task<ActionResult<List<Branch>>> AddBranch(Branch newBranch)
        {
            return await _repo.AddBranch(newBranch);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Branch>> UpdateBranch(int id, BranchDTO newBranch)
        {
            return await _repo.UpdateBranch(id, newBranch);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<string>> DeleteBranch(int id)
        {
            bool checkDelete = await _repo.DeleteBranch(id);
            if (checkDelete == true)
            {
                return $"Delete branch with id: {id} successfully";
            }
            else
            {
                return $"Delete branch with id: {id} unsuccessfully";
            }
        }
    }
}
