﻿using SaleManagement.DTO;
using SaleManagement.Models;

namespace SaleManagement.Repositories
{
    public interface IBranchRepository
    {
        Task<List<Branch>> GetAll();
        Task<List<Branch>> GetById(int id);
        Task<List<Branch>> AddBranch(Branch branch);
        Task<Branch> UpdateBranch(int id, BranchDTO branch);
        Task<bool> DeleteBranch(int id);
    }
}
