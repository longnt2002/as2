﻿using Microsoft.EntityFrameworkCore;
using SaleManagement.DTO;
using SaleManagement.Models;

namespace SaleManagement.Repositories
{
    public class BranchRepository : IBranchRepository
    {
        private readonly SaleManagementContext _context;

        public BranchRepository(SaleManagementContext context)
        {
            _context = context;
        }
        public async Task<List<Branch>> AddBranch(Branch branch)
        {
            var notIdUnique = await _context.Branches.Where(b => b.BranchId == branch.BranchId).FirstOrDefaultAsync();
            if (notIdUnique != null)
            {
                throw new ArgumentException("Branch id must be unique", nameof(branch.BranchId));
            }
            else
            {
                await _context.Branches.AddAsync(branch);
                await _context.SaveChangesAsync();
            }

            return await GetAll();
        }


        public async Task<bool> DeleteBranch(int id)
        {
            var deletedBranch = await _context.Branches.Where(x => x.BranchId == id).FirstOrDefaultAsync();
            if (deletedBranch != null)
            {
                _context.Branches.Remove(deletedBranch);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public async Task<List<Branch>> GetAll()
        {
            return await _context.Branches.ToListAsync();
        }

        public async Task<List<Branch>> GetById(int id)
        {
            var branch = await _context.Branches.Where(x => x.BranchId == id).Distinct().ToListAsync();
            if (branch != null)
            {
                return branch;
            }
            return null;
        }

        public async Task<Branch> UpdateBranch(int id, BranchDTO branch)
        {
            var updatedBranch = await _context.Branches.Where(x => x.BranchId == id).FirstOrDefaultAsync();
            if (updatedBranch != null)
            {
                updatedBranch.Name = branch.Name;
                updatedBranch.Address = branch.Address;
                updatedBranch.City = branch.City;
                updatedBranch.State = branch.State;
                updatedBranch.Zipcode = branch.Zipcode;
                var tracker = _context.Attach(updatedBranch);
                tracker.State = EntityState.Modified;
                _context.SaveChanges();
                return updatedBranch;
            }
            return null;
        }
    }
}
